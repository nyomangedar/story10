from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve
from .views import SignUp
from django.contrib.auth.forms import UserCreationForm
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import unittest

class UnitTest(TestCase):
    def test_urls_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_view_use_correct_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_inside_html(self):
        response=Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("You are not logged in yet", response_content)

    def test_form_validation_blank_items(self):
        form = UserCreationForm(data={'username':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'],
            ["This field is required."]
        )

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()
    
    def test_url(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
    
    def test_sign_up_check(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        self.assertIn('You are not logged in yet', selenium.page_source)
        
        #SignUp
        signup = selenium.find_element_by_id('signup')
        signup.click()
        selenium.implicitly_wait(10)
        username = selenium.find_element_by_id('id_username')
        username.send_keys("testcase_selenium")
        password1 = selenium.find_element_by_id('id_password1')
        password1.send_keys('arabianplayboy')
        password2 = selenium.find_element_by_id('id_password2')
        password2.send_keys('arabianplayboy')
        button = selenium.find_element_by_id('submit')
        button.click()
        selenium.implicitly_wait(10)
        self.assertIn('Login Here', selenium.page_source)

        #Login
        username = selenium.find_element_by_id('id_username')
        username.send_keys('testcase_selenium')
        password = selenium.find_element_by_id('id_password')
        password.send_keys('arabianplayboy')
        button = selenium.find_element_by_id('submit')
        button.click()
        selenium.implicitly_wait(10)
        self.assertIn('Hello and Welcome testcase_selenium', selenium.page_source)

        #Logout
        button = selenium.find_element_by_id('logout')
        button.click()
        selenium.implicitly_wait(10)
        self.assertIn('You are not logged in yet', selenium.page_source)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()



    



    
































# from django.test import TestCase
# from myapp.models import Status


# # models test
# class WhateverTest(TestCase):

#     def create_whatever(self, title="only a test", body="yes, this is only a test"):
#         return Status.objects.create(status=status)

#     def test_whatever_creation(self):
#         w = self.create_whatever()
#         self.assertTrue(isinstance(w, Status))
#         self.assertEqual(w.__unicode__(), w.title)
