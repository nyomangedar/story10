from django.urls import path, include
from app.views import SignUp
from django.views.generic.base import TemplateView

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', TemplateView.as_view(template_name='index.html'), name='home'),
    path('signup/', SignUp.as_view(), name='signup'),

]


# urlpatterns =[
#     path('', views.home, name='home'),
#     path('signup', views.signUp, name='signUp'),
#     path('login', views.login, name='login'),
#     path('/confirm', views.confirm, name='confirm'),
# ]